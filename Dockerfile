FROM node:9.3-slim

LABEL name="Freebie API Server"

RUN mkdir /server $$ echo "Server directory is created"

WORKDIR /server

COPY package.json .

RUN apt-get update && apt-get install -y build-essential && apt-get install -y python && npm install

COPY . .

RUN cp docker-entrypoint.sh /usr/local/bin/ && \chmod +x /usr/local/bin/docker-entrypoint.sh

EXPOSE 9100

ENTRYPOINT [ "docker-entrypoint.sh" ]