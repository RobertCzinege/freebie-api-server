'use strict'

const logger = require('winston')
const knex = require('../models/db/')
const { generateProducts } = require('../utils/generate-seed')
const retry = require('../utils/retry')
const { hashPass } = require('../models/auth/index')

async function checkConnection() {
  try {
    await retry(10, knex.select.bind(knex.select), { criteria: 1 })
    logger.info('DB connection is ready')
    return
  } catch (err) {
    logger.error(err)
    throw err
  }
}

async function migrateDB() {
  await knex.migrate.latest()
    .then(() => {
      // eslint-disable-next-line no-console
      logger.info('DB is synched')
    })
    .catch((err) => {
      // eslint-disable-next-line no-console
      logger.error(err)
      throw err
    })
}

async function seedProducts(quantity) {
  const fakeProducts = generateProducts(quantity)

  try {
    const id = await knex.batchInsert('products', fakeProducts)
      .returning('id')
    logger.debug(`Successfully created products: ${id}`)
  } catch (err) {
    logger.error(err)
    throw err
  }
}

async function seedAdmin() {
  try {
    const adminUser = {
      username: 'admin',
      email: 'admin@admin.com',
      pass: await hashPass('admin'),
      isAdmin: true
    }

    const result = await knex('users').insert(adminUser).returning('*')
    logger.debug(result)
  } catch (err) {
    logger.error(err)
    throw err
  }
}

async function seedUser() {
  try {
    const user = {
      username: 'User Doe',
      email: 'user@user.com',
      pass: await hashPass('12345'),
      isAdmin: false
    }

    const result = await knex('users').insert(user).returning('*')
    logger.debug(result)
  } catch (err) {
    logger.error(err)
    throw err
  }
}

async function clearDB() {
  try {
    await knex.migrate.rollback()
    logger.debug('Database is cleared')
    return
  } catch (err) {
    logger.error('Cannot drop the base')
    throw err
  }
}

async function seedDB() {
  try {
    await seedProducts(20)
    await seedAdmin()
    await seedUser()
    return
  } catch (err) {
    logger.error(err)
    throw err
  }
}

async function checkDBContent() {
  try {
    const result = await knex('products').first()
    if (result) {
      return false
    }
    return true
  } catch (err) {
    throw err
  }
}

module.exports = {
  checkConnection,
  migrateDB,
  seedDB,
  clearDB,
  checkDBContent
}
