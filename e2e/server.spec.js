'use strict'

const request = require('super-request')
const { expect } = require('chai')
const server = require('../processes/web/server')

describe('GET /healthcheck', () => {
  it('should response with `I\'m alive!`', async () => {
    const { body } = await request(server.listen())
      .get('/api/healthcheck')
      .expect(200)
      .end()

    expect(body).to.eql('I\'m alive!')
  })
})
