'use strict'

const request = require('super-request')
const { expect } = require('chai')
const server = require('../../processes/web/server')
const UserDB = require('../../models/db/users')

const url = '/api/users/register'

const testUserRequest = {
  username: 'John Doe',
  email: 'john.doe@coolmail.com',
  pass: '1234Pass'
}

const failRequest = {
  a: 'a',
  b: 'b',
  pass: 'c'
}

describe('POST /api/users/register', () => {
  it('should respond with 201 on success', async function t() {
    this.sandbox.stub(UserDB, 'insert').resolves()

    const { body } = await request(server.listen())
      .post(url)
      .json(testUserRequest)
      .expect(201)
      .end()

    expect(body).to.eql({
      message: 'User successfully registered'
    })
  })

  it('should return error 400 on error', async function t() {
    this.sandbox.stub(UserDB, 'insert').rejects()

    await request(server.listen())
      .post(url)
      .json(failRequest)
      .expect(400)
      .end()
  })

  it('should return with error if the request body is not proper', async () => {
    await request(server.listen())
      .post(url)
      .json(failRequest)
      .expect(400)
      .end()
  })
})
