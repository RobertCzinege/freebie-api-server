'use strict'

const request = require('super-request')
const { expect } = require('chai')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const server = require('../../processes/web/server')
const UserDB = require('../../models/db/users')

const url = '/api/users/login'

const testUserRequest = {
  email: 'john.doe@coolmail.com',
  pass: '1234Pass'
}

const failRequest = {
  a: 'a',
  b: 'b'
}

const dbResult = {
  id: 1,
  username: 'john doe',
  email: testUserRequest.email
}

const response = {
  token: 'test',
  message: 'Successfully logged in',
  user: {
    username: 'john doe',
    email: testUserRequest.email
  }
}

describe('POST /api/users/login', () => {
  it('should respond with 200 on success', async function t() {
    this.sandbox.stub(UserDB, 'getLogin').resolves(dbResult)
    this.sandbox.stub(bcrypt, 'compare').resolves(true)
    this.sandbox.stub(jwt, 'sign').returns(response.token)

    const { body } = await request(server.listen())
      .post(url)
      .json(testUserRequest)
      .expect(200)
      .end()

    expect(body).to.eql(response)
  })

  it('should return error 400 if request is improper', async function t() {
    this.sandbox.stub(UserDB, 'getLogin').resolves(dbResult)
    this.sandbox.stub(bcrypt, 'compare').resolves(true)
    this.sandbox.stub(jwt, 'sign').returns(response.token)

    await request(server.listen())
      .post(url)
      .json(failRequest)
      .expect(400)
      .end()
  })

  it('should return with 401 if the user is valid, but the password is incorrect', async function t() {
    this.sandbox.stub(UserDB, 'getLogin').resolves(dbResult)
    this.sandbox.stub(bcrypt, 'compare').resolves(false)

    await request(server.listen())
      .post(url)
      .json(testUserRequest)
      .expect(401)
      .end()
  })

  it('should return with 401 if the user is not in the db', async function t() {
    this.sandbox.stub(UserDB, 'getLogin').resolves(undefined)
    // this.sandbox.stub(bcrypt, 'compare').resolves(false)

    await request(server.listen())
      .post(url)
      .json(testUserRequest)
      .expect(401)
      .end()
  })

  it('should return with 500 if error happens', async function t() {
    this.sandbox.stub(UserDB, 'getLogin').resolves(dbResult)
    this.sandbox.stub(bcrypt, 'compare').rejects()

    await request(server.listen())
      .post(url)
      .json(testUserRequest)
      .expect(500)
      .end()
  })
})
