'use strict'

const request = require('super-request')
const { expect } = require('chai')
const server = require('../../processes/web/server')
const ProductsDB = require('../../models/db/products')

const urlAll = '/api/products'
const url = '/api/products/123'
const queryResult = [
  {
    id: 1,
    name: 'test1',
    description: 'test',
    ingredients: 'ing1, ing2',
    price: 1.0,
    currency: 'USD',
    available: true
  },
  {
    id: 2,
    name: 'test2',
    description: 'test2',
    ingredients: 'ing3, ing4',
    price: 2.0,
    currency: 'USD',
    available: false
  }
]

describe('GET /api/products', () => {
  it('should respond with 200 on success and with the list of products', async function t() {
    this.sandbox.stub(ProductsDB, 'getAllProducts').resolves(queryResult)

    const { body } = await request(server.listen())
      .get(urlAll)
      .expect(200)
      .end()

    expect(JSON.parse(body)).to.be.eql({ products: queryResult })
  })

  it('should return with 200 on success with the filtered list of products', async function t() {
    this.sandbox.stub(ProductsDB, 'getProductsBySearch').resolves([queryResult[0]])

    const { body } = await request(server.listen())
      .get(urlAll)
      .qs({
        search: 'test1'
      })
      .expect(200)
      .end()

    expect(JSON.parse(body)).to.be.eql({ products: [queryResult[0]] })
  })

  it('should return with the given product', async function t() {
    this.sandbox.stub(ProductsDB, 'getProductByID').resolves([queryResult[0]])

    const { body } = await request(server.listen())
      .get(url)
      .expect(200)
      .end()

    expect(JSON.parse(body)).to.be.eql(queryResult[0])
  })

  it('should return with 404 if specific product is not found', async function t() {
    this.sandbox.stub(ProductsDB, 'getProductByID').resolves([])

    await request(server.listen())
      .get(url)
      .expect(404)
      .end()
  })

  it('should return with 500 in case of error', async function t() {
    this.sandbox.stub(ProductsDB, 'getProductByID').rejects()

    await request(server.listen())
      .get(url)
      .expect(500)
      .end()
  })
})
