'use strict'

const request = require('super-request')
const jwt = require('jsonwebtoken')
const server = require('../../processes/web/server')
const ProductsDB = require('../../models/db/products')

const url = '/api/products/123'
const veryficationAdmin = {
  id: 1,
  isAdmin: true
}
const veryficationRegular = {
  id: 1,
  isAdmin: false
}

describe('DELETE /api/products/:id', () => {
  it('should respond with 200 on success with admin user request', async function t() {
    this.sandbox.stub(ProductsDB, 'delete').resolves(1)
    this.sandbox.stub(jwt, 'verify').returns(veryficationAdmin)

    await request(server.listen())
      .del(url)
      .expect(200)
      .end()
  })

  it('should return error 404 if there is nothing to delete', async function t() {
    this.sandbox.stub(ProductsDB, 'delete').resolves(0)
    this.sandbox.stub(jwt, 'verify').returns(veryficationAdmin)

    await request(server.listen())
      .del(url)
      .expect(404)
      .end()
  })

  it('should return with 401 if the user is not admin', async function t() {
    this.sandbox.stub(ProductsDB, 'delete').resolves(1)
    this.sandbox.stub(jwt, 'verify').returns(veryficationRegular)

    await request(server.listen())
      .del(url)
      .expect(401)
      .end()
  })

  it('should return with 500 if error happens', async function t() {
    this.sandbox.stub(ProductsDB, 'delete').rejects(0)
    this.sandbox.stub(jwt, 'verify').returns(veryficationAdmin)

    await request(server.listen())
      .del(url)
      .expect(500)
      .end()
  })
})
