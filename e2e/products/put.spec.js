'use strict'

const request = require('super-request')
const { expect } = require('chai')
const server = require('../../processes/web/server')
const ProductsDB = require('../../models/db/products')
const jwt = require('jsonwebtoken')

const url = '/api/products/123'
const queryResult = [
  {
    id: 1,
    name: 'test1',
    description: 'test',
    ingredients: 'ing1, ing2',
    price: 1.0,
    currency: 'USD',
    available: true
  }
]
const requestBody = {
  name: 'test1',
  description: 'test',
  ingredients: 'ing1, ing2',
  price: 1.0,
  currency: 'USD',
  available: true
}
const faultyRequest = {
  a: 'b',
  b: 'a'
}
const veryficationAdmin = {
  id: 1,
  isAdmin: true
}
const veryficationRegular = {
  id: 1,
  isAdmin: false
}

describe('PUT /api/products', () => {
  it('should respond with 200 on success and with the list of products', async function t() {
    this.sandbox.stub(ProductsDB, 'edit').resolves(queryResult[0])
    this.sandbox.stub(jwt, 'verify').returns(veryficationAdmin)

    const { body } = await request(server.listen())
      .put(url)
      .json(requestBody)
      .expect(200)
      .end()

    expect(body).to.be.eql(queryResult[0])
  })

  it('should return with 400 in case of improper request', async function t() {
    this.sandbox.stub(ProductsDB, 'edit').resolves(queryResult[0])
    this.sandbox.stub(jwt, 'verify').returns(veryficationAdmin)

    await request(server.listen())
      .put(url)
      .json(faultyRequest)
      .expect(400)
      .end()
  })

  it('should return with 500 in case of error', async function t() {
    this.sandbox.stub(ProductsDB, 'edit').rejects()
    this.sandbox.stub(jwt, 'verify').returns(veryficationAdmin)

    await request(server.listen())
      .put(url)
      .json(requestBody)
      .expect(500)
      .end()
  })

  it('should return with 401 if user is not authorized', async function t() {
    this.sandbox.stub(ProductsDB, 'edit').resolves()
    this.sandbox.stub(jwt, 'verify').returns(veryficationRegular)

    await request(server.listen())
      .put(url)
      .json(requestBody)
      .expect(401)
      .end()
  })
})
