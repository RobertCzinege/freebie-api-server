'use strict'

const ProductsDB = require('../../../../models/db/products')
const logger = require('winston')
const joi = require('joi')
const compose = require('koa-compose')
const validator = require('../../middlewares/requestValidator')

const paramsSchema = joi.object({
  id: joi.number().integer().min(0).required()
})
  .required()

const GetProducts = {
  async productByID(ctx) {
    try {
      const [result] = await ProductsDB.getProductByID(ctx.params.id)
      if (!result) {
        ctx.status = 404
        return
      }
      ctx.body = result
      ctx.status = 200
    } catch (err) {
      logger.error(err)
      ctx.status = 500
    }
  },

  async listAllProducts(ctx) {
    try {
      if (ctx.query.search) {
        ctx.body = Object.assign({}, { products: await ProductsDB.getProductsBySearch(ctx.query.search) })
      } else {
        ctx.body = Object.assign({}, { products: await ProductsDB.getAllProducts() })
      }
      ctx.status = 200
    } catch (err) {
      logger.error(err)
      ctx.status = 500
    }
  }
}


module.exports = {
  listAllProducts: GetProducts.listAllProducts,
  listProductByID: compose([
    validator({
      params: paramsSchema
    }),
    GetProducts.productByID
  ]),
  GetProducts
}
