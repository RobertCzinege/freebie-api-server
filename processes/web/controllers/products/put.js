'use strict'

const ProductsDB = require('../../../../models/db/products')
const logger = require('winston')
const joi = require('joi')
const compose = require('koa-compose')
const validator = require('../../middlewares/requestValidator')

const bodySchema = joi.object({
  id: joi.number().optional(),
  name: joi.string().optional(),
  description: joi.string().optional(),
  ingredients: joi.string().optional(),
  price: joi.number().optional(),
  currency: joi.string().optional(),
  available: joi.boolean().optional()
})
  .required()

const EditProduct = {
  async editProduct(ctx) {
    try {
      ctx.body = await ProductsDB.edit(ctx.params.id, ctx.request.body)
      ctx.status = 200
    } catch (err) {
      logger.error(err)
      ctx.status = 500
    }
  }
}


module.exports = {
  editProduct: compose([
    validator({
      body: bodySchema
    }),
    EditProduct.editProduct
  ]),
  EditProduct
}
