'use strict'

const ProductsDB = require('../../../../models/db/products')
const logger = require('winston')
const joi = require('joi')
const compose = require('koa-compose')
const validator = require('../../middlewares/requestValidator')

const paramsSchema = joi.object({
  id: joi.number().integer().min(1).required()
})
  .required()

async function deleteProduct(ctx) {
  try {
    const result = await ProductsDB.delete(ctx.params.id)
    if (result === 1) {
      ctx.status = 200
    } else {
      ctx.status = 404
    }
  } catch (err) {
    logger.error(err)
    ctx.status = 500
  }
}


module.exports = {
  deleteProduct: compose([
    validator({
      params: paramsSchema
    }),
    deleteProduct
  ])
}
