'use strict'

const { listAllProducts, listProductByID } = require('./get')
const { addNewProduct } = require('./post')
const { editProduct } = require('./put')
const { deleteProduct } = require('./delete')

module.exports = {
  listAllProducts,
  listProductByID,
  addNewProduct,
  editProduct,
  deleteProduct
}
