'use strict'

const ProductsDB = require('../../../../models/db/products')
const logger = require('winston')
const joi = require('joi')
const compose = require('koa-compose')
const validator = require('../../middlewares/requestValidator')

const bodySchema = joi.object({
  name: joi.string().required(),
  description: joi.string().optional(),
  ingredients: joi.string().optional(),
  price: joi.number().required(),
  currency: joi.string().required(),
  available: joi.boolean().required()
})
  .required()

const AddProduct = {
  async addNewProduct(ctx) {
    try {
      ctx.body = await ProductsDB.insertProduct(ctx.request.body)
      ctx.status = 201
    } catch (err) {
      logger.error(err)
      ctx.status = 500
    }
  }
}

module.exports = {
  addNewProduct: compose([
    validator({
      body: bodySchema
    }),
    AddProduct.addNewProduct
  ]),
  AddProduct
}
