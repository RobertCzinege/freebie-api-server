'use strict'

const products = require('./products/index')
const users = require('./user-management/index')

module.exports = {
  products,
  users
}
