'use strict'

const logger = require('winston')
const compose = require('koa-compose')
const joi = require('joi')
const UserDB = require('../../../../models/db/users')
const { hashPass } = require('../../../../models/auth/index')
const validator = require('../../middlewares/requestValidator')

const bodySchema = joi.object({
  username: joi.string().required(),
  email: joi.string().email().required(),
  pass: joi.string().required()
})
  .required()

const UserRegister = {
  async register(ctx) {
    try {
      const credentials = {
        username: ctx.request.body.username,
        email: ctx.request.body.email,
        pass: await hashPass(ctx.request.body.pass)
      }
      await UserDB.insert(credentials)
      ctx.body = {
        message: 'User successfully registered'
      }
      ctx.status = 201
    } catch (err) {
      logger.error(err)
      ctx.status = 500
    }
  }
}


module.exports = {
  register: compose([
    validator({
      body: bodySchema
    }),
    UserRegister.register
  ])
}
