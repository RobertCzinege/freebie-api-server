'use strict'

const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const logger = require('winston')
const compose = require('koa-compose')
const joi = require('joi')
const UserDB = require('../../../../models/db/users')
const validator = require('../../middlewares/requestValidator')

const bodySchema = joi.object({
  email: joi.string().email().required(),
  pass: joi.string().required()
})
  .required()

const UserLogin = {
  async login(ctx) {
    const loginCredentials = {
      email: ctx.request.body.email,
      password: ctx.request.body.pass
    }

    try {
      const result = await UserDB.getLogin(loginCredentials.email)
      if (!result) {
        ctx.status = 401
        return
      }
      const isPasswordValid = await bcrypt.compare(loginCredentials.password, result.pass)
      if (!isPasswordValid) {
        ctx.status = 401
        return
      }
      const token = jwt.sign({ id: result.id, isAdmin: result.isAdmin }, process.env.TOKEN_SECRET, {
        expiresIn: process.env.TOKEN_EXPIRATION
      })
      ctx.body = {
        token,
        message: 'Successfully logged in',
        user: {
          username: result.username,
          email: result.email,
          admin: result.isAdmin
        }
      }
      return
    } catch (err) {
      logger.error(err)
      ctx.status = 500
    }
  }
}


module.exports = {
  login: compose([
    validator({
      body: bodySchema
    }),
    UserLogin.login
  ])
}
