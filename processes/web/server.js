'use strict'

const Koa = require('koa')
const logger = require('winston')
const router = require('./router')

const app = new Koa()
const cors = require('koa2-cors')

app.proxy = true

app
  .use(cors({
    origin: '*'
  }))
  .use(router.public.routes())
  .use(router.secured.logged.routes())
  .use(router.secured.admin.routes())
  .use(router.public.allowedMethods())

app.on('error', (err) => {
  logger.error('Server error', { error: err.message })
  throw err
})

module.exports = app
