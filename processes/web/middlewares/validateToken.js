'use strict'

const jwt = require('jsonwebtoken')

async function validateToken(ctx, next) {
  const secret = process.env.TOKEN_SECRET
  const requestToken = ctx.request.headers.token

  try {
    const payload = jwt.verify(requestToken, secret)

    ctx.user = {
      id: payload.id,
      isAdmin: payload.isAdmin
    }
  } catch (err) {
    ctx.status = 401
    return
  }
  // eslint-disable-next-line
  return next()
}

module.exports = {
  validateToken
}
