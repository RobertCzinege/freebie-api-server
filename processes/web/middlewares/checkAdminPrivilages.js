'use strict'

// eslint-disable-next-line
async function checkAdminPrivilages(ctx, next) {
  if (ctx.user.isAdmin) {
    return next()
  }
  ctx.status = 401
}

module.exports = checkAdminPrivilages
