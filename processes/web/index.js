'use strict'

const dotenv = require('dotenv')

if (process.env.NODE_ENV === 'development') {
  dotenv.config({ silent: true })
}

const { promisify } = require('util')
const http = require('http')
const logger = require('winston')
const app = require('./server')
const config = require('./config/config')
const {
  seedDB,
  checkConnection,
  migrateDB,
  clearDB,
  checkDBContent
} = require('../../scripts/seed-db')

const server = http.createServer(app.callback())
const serverListen = promisify(server.listen).bind(server)

// Config logger
logger.default.transports.console.colorize = true
logger.level = config.logger.level

if (process.env.NODE_ENV === 'development') {
  checkConnection()
    .then(() => clearDB())
    .then(() => migrateDB())
    .then(() => seedDB())
    .then(() => serverListen(config.port))
    .then(() => {
      logger.info(`Server is up!
      You can reach the server on localhost:${config.port}`)
    })
    .catch((err) => {
      logger.error(err)
      process.exit(1)
    })
} else {
  checkConnection()
    .then(() => migrateDB())
    .then(() => checkDBContent())
    .then((isEmpty) => {
      if (isEmpty) {
        return seedDB()
      }
      return Promise.resolve()
    })
    .then(() => {
      logger.info('DB is seeded and the server is up!')
    })
    .catch((err) => {
      logger.error('Error happened during DB initializing', err.message)
      process.exit(1)
    })
    .then(() => serverListen(config.port))
    .then(() => {
      logger.info(`Server is up!
      You can reach the server on localhost:${config.port}`)
    })
    .catch((err) => {
      logger.error('Error happened on server start', err.message)
      process.exit(1)
    })
}

