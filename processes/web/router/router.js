'use strict'

const Router = require('koa-router')
const bodyParser = require('koa-bodyparser')
const { products, users } = require('../controllers/index')
const { validateToken } = require('../middlewares/validateToken')
const checkAdminPrivilages = require('../middlewares/checkAdminPrivilages')

const router = {
  public: new Router(),
  secured: {
    logged: new Router(),
    admin: new Router()
  }
}

router.public.use(bodyParser())

router.secured.logged.use(bodyParser())
router.secured.logged.use(validateToken)

router.secured.admin.use(bodyParser())
router.secured.admin.use(validateToken)
router.secured.admin.use(checkAdminPrivilages)


router.public.get('/api/healthcheck', (ctx) => {
  ctx.body = 'I\'m alive!'
})

// USER MANAGEMENT
router.public.post('/api/users/register', users.register)
router.public.post('/api/users/login', users.login)

// PRODUCTS
router.public.get('/api/products', products.listAllProducts)
router.public.get('/api/products/:id', products.listProductByID)

router.secured.admin.put('/api/products/:id', products.editProduct)
router.secured.admin.post('/api/products', products.addNewProduct)
router.secured.admin.delete('/api/products/:id', products.deleteProduct)

module.exports = router
