'use strict'

const { hashPass } = require('./hashPass')

module.exports = {
  hashPass
}
